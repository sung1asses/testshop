<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->unique()->sentence;
    $slug = Str::slug($name, '-');
    return [
        'name' => $name,
        'slug' => $slug,
        'image' => $faker->imageUrl(),
        'description' => $faker->text,
        'price' => $faker->numberBetween(1000,9999),
        'category_id'=> $faker->randomDigitNotNull,
    ];
});
