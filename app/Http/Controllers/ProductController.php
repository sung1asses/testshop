<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    public function index(){
        $products = Product::get();
        if(!$products){
            return response([
                'message' => 'Продуктов нет!'
            ], 400);
        }
        $response = new ProductCollection(
            $products
        );
        return response($response, 200);
    }

    public function show($id){
        $product = Product::find($id);
        if(!$product){
            return response([
                'message' => 'Продукта с таким {id} не существует!'
            ], 400);
        }
        $response = new ProductResource(
            $product
        );
        return response($response, 200);
    }

}
