<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::with('products')->get();
        if(!$categories){
            return response([
                'message' => 'Продуктов нет!'
            ], 400);
        }
        $response = new CategoryCollection(
            $categories
        );
        return response($response, 200);
    }

    public function showById($id){
        $category = Category::with('products')->find($id);
        if(!$category){
            return response([
                'message' => 'Категории с таким {id} не существует!'
            ], 400);
        }
        $response = new CategoryResource(
            $category
        );
        return response($response, 200);
    }

    public function showBySlug($slug){
        $category = Category::with('products')->where('slug', $slug)->first();
        if(!$category){
            return response([
                'message' => 'Категории с таким {slug} не существует!'
            ], 400);
        }
        $response = new CategoryResource(
            $category
        );
        return response($response, 200);
    }
}
